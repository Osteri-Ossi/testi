<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Oskar Kallunki">
    <title>Nettopalkkalaskuri</title>
</head>

<body>
    <h2 style="display: block; padding-bottom: 10px; padding-left: 10px;">Nettopalkkalaskuri</h2>
    <form action="<?php print($_SERVER['PHP_SELF']); ?>" method="post" style="max-width: 35%;">
        <div style="display: block; padding: 10px;">
            <label for=" brutto">Bruttopalkka</label>
            <input name=" brutto" type="number" step="0.01" required style="float: right;" placeholder="€">
        </div>
        <div  style="display: block; padding: 10px;">
            <label for="ennakko">Ennakkopidätys</label>
            <input name="ennakko" type="number" step="0.01" required style="float: right;" placeholder="%">
        </div>
        <div  style="display: block; padding: 10px;">
            <label for="elake">Työeläkemaksu</label>
            <input name="elake" type="number" step="0.01" required style="float: right;" placeholder="%">
        </div>
        <div style="display: block; padding: 10px;">
            <label for="tyottomyys">Työttömyysvakuutusmaksu</label>
            <input name="tyottomyys" type="number" step="0.01" required style="float: right;" placeholder="%">
        </div>
        <div style="display: block; padding: 10px;">
            <button>Laske</button>
        </div>
        <?php
        if ($_SERVER['REQUEST_METHOD']=='POST') {
            $brutto = filter_input(INPUT_POST, "brutto", FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
            $ennakko = filter_input(INPUT_POST, "ennakko", FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION) / 100;
            $elake= filter_input(INPUT_POST, "elake", FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION) / 100;
            $tyottomyys = filter_input(INPUT_POST, "tyottomyys", FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION) / 100;
            $netto = $brutto - ($brutto * $ennakko) - ($brutto * $elake) - ($brutto * $tyottomyys);
            printf("<p style='display: inline-block; padding: 3px; margin-left: 10px; background-color: pink;'>Ennakkopidätys on %.2f €</p><br>", $brutto * $ennakko);
            printf("<p style='display: inline-block; padding: 3px; margin-left: 10px; background-color: pink;'>Työeläkemaksu on %.2f €</p><br>", $brutto * $elake);
            printf("<p style='display: inline-block; padding: 3px; margin-left: 10px; background-color: pink;'>Työttömyysvakuutusmaksu on %.2f €</p><br>", $brutto * $tyottomyys);
            printf("<p style='display: inline-block; padding: 3px; margin-left: 10px; font-weight: bold; background-color: lightgreen;'>Nettopalkka on %.2f €</p><br>", $netto);
        }
        ?>
    </form>

</body>

</html>